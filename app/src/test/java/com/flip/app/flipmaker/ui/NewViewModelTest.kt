package com.flip.app.flipmaker.ui

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.flip.app.flipmaker.ui.trip.NewViewModel
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations

class NewViewModelTest {

    private lateinit var viewModel: NewViewModel

    private val journey = "test"

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        viewModel = NewViewModel()
    }

    @Test
    fun `When journey is not empty isChecker should be true`() {

        viewModel.setChecker(journey)

        assertTrue(viewModel.isChecker.value!!)
    }

    @Test
    fun `When journey is empty isChecker should be false`() {

        viewModel.setChecker("")

        assertFalse(viewModel.isChecker.value!!)
    }

}