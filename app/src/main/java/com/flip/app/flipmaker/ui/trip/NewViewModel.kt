package com.flip.app.flipmaker.ui.trip

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log


class NewViewModel: ViewModel() {

    var title = MutableLiveData<String>()

    var isChecker = MutableLiveData<Boolean>()

    val startWelcomeActivity = MutableLiveData<Boolean>()

    init {
        isChecker.value = false
    }

    fun setChecker(journey: String) {
        isChecker.value = !journey.isEmpty()
    }

    fun saveJourney() {
        startWelcomeActivity.value = true
    }
}