package com.flip.app.flipmaker.ui.welcome

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log

class WelcomeViewModel : ViewModel() {


    val name = MutableLiveData<String>()
    val topic = MutableLiveData<String>()

    init {
    }

    fun onNameChanged(name: String) {
        topic.value = name
    }

}