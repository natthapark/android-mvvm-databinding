package com.flip.app.flipmaker.utils

import android.support.annotation.NonNull
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction

class ActivityUtils {

    companion object {
        fun addFragmentToActivity (@NonNull fragmentManager: FragmentManager,
                                   @NonNull fragment: Fragment,
                                   containerId: Int) {
            checkNotNull(fragmentManager)
            checkNotNull(fragment)

            val ft: FragmentTransaction = fragmentManager.beginTransaction()
            ft.add(containerId, fragment)
            ft.commit()
        }
    }
}