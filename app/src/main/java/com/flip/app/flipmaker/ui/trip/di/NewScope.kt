package com.flip.app.flipmaker.ui.trip.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class NewScope