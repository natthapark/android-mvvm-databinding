package com.flip.app.flipmaker.ui.welcome

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.DataBindingUtil.inflate
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.flip.app.flipmaker.R
import com.flip.app.flipmaker.databinding.FragmentWelcomeBinding
import kotlinx.android.synthetic.main.fragment_welcome.*


class WelcomeFragment : Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(WelcomeViewModel::class.java)
    }

    public override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                                     savedInstanceState: Bundle?): View? {

        val binding: FragmentWelcomeBinding =
                inflate(inflater!!, R.layout.fragment_welcome, container, false)
        binding.viewModel = viewModel
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object {
        fun newInstance(): WelcomeFragment  = WelcomeFragment()
    }
}
