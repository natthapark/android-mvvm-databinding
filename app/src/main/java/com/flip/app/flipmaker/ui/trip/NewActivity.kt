package com.flip.app.flipmaker.ui.trip

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.flip.app.flipmaker.R
import com.flip.app.flipmaker.utils.ActivityUtils



class NewActivity : AppCompatActivity() {

    private lateinit var viewModel: NewViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)

        setupFragment()
    }

    private fun setupFragment() {
        ActivityUtils.addFragmentToActivity(
                supportFragmentManager,
                NewFragment.newInstance(),
                R.id.contentContainer
        )
    }
}
