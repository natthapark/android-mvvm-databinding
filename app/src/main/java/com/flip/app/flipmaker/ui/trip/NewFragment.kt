package com.flip.app.flipmaker.ui.trip

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.flip.app.flipmaker.R
import com.flip.app.flipmaker.ui.welcome.WelcomeActivity

import kotlinx.android.synthetic.main.fragment_new.*

class NewFragment : Fragment() {

    private val journey: String
        get() = edtJourney.text.toString()


    private val viewModel by lazy {
        ViewModelProviders.of(this).get(NewViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_new, container, false);
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupSubscriber()
        setupJourneyTextChangedListener()
        setupOnClickListener()
    }

    private fun setupSubscriber() {

        /**
         *  Subscribe to set checker
         */
        viewModel.isChecker.observe(this, Observer<Boolean> {
            it?.let {
                if (it) {
                    imgChecker.apply {
                        setImageResource(R.drawable.icon_checkmark_white)
                        isClickable = true
                    }

                } else {
                    imgChecker.apply {
                        setImageResource(R.drawable.icon_checkmark_grey)
                        isClickable = false
                    }
                }

            }
        })

        /**
         *  Subscribe to start WelcomeActivity
         */
        viewModel.startWelcomeActivity.observe(this, Observer<Boolean>{
            it?.let {
                if (it) {
                    startWelcomeActivity()
                }

            }
        })
    }

    private fun setupJourneyTextChangedListener() {
        edtJourney.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                viewModel.setChecker(journey)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    private fun setupOnClickListener() {
        imgChecker.setOnClickListener {
            viewModel.saveJourney()
        }
    }

    private fun startWelcomeActivity() {
        val intent = Intent(context, WelcomeActivity::class.java)
        startActivity(intent)
    }

    companion object {

        fun newInstance(): NewFragment {
            val fragment = NewFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

}
