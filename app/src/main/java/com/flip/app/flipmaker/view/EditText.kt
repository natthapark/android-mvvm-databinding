package com.flip.app.flipmaker.view

import android.content.Context
import android.graphics.Typeface
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import com.flip.app.flipmaker.R

class EditText : AppCompatEditText {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setCustomFont(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        setCustomFont(context, attrs)
    }


    private fun setCustomFont(context: Context, attrs: AttributeSet?) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.Font)
        val font = a.getString(R.styleable.Font_typeface)
        if (font != null) {
            setCustomFont(context, font)
            a.recycle()
        }

    }

    private fun setCustomFont(context: Context, font: String) {
        var typeface: Typeface? = null
        try {
            typeface = Typeface.createFromAsset(context.assets, font)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        setTypeface(typeface)
    }

}