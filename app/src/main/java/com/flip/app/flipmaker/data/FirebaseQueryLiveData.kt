package com.flip.app.flipmaker.data

import android.arch.lifecycle.LiveData
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.Query
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener



class FirebaseQueryLiveData(private val query: Query) : LiveData<DataSnapshot>() {

    private val listener = MyValueEventListener()

    override fun onActive() {
        query.addValueEventListener(listener)
    }

    override fun onInactive() {
        query.removeEventListener(listener)
    }

    private inner class MyValueEventListener : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            setValue(dataSnapshot)
        }

        override fun onCancelled(databaseError: DatabaseError) {

        }
    }
}