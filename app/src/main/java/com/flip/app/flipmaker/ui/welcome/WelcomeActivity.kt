package com.flip.app.flipmaker.ui.welcome

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.flip.app.flipmaker.R
import com.flip.app.flipmaker.utils.ActivityUtils
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        setupFragment()
    }

    private fun setupFragment() {
        ActivityUtils.addFragmentToActivity(
                supportFragmentManager,
                WelcomeFragment.newInstance(),
                welcomeContainer.id)
    }
}
